from datetime import datetime

class Wydatek:
    def __init__(self,nazwa,kwota,typ="towar",data = datetime):
        self.nazwa = nazwa
        self.kwota = kwota
        self.typ = typ
        self.data = data
    def __str__(self):
            return(self.nazwa + " " + str(self.kwota) + " " + self.typ + " " + str(self.data.year) + "." + str(self.data.month) + "." + str(self.data.day))# klasa Lista z atrybutem lista_produktow przechowująca każdy dodany wydatek
class Lista_wydatkow:
    def __init__(self,lista_produktow):
        self.lista_produktow = lista_produktow    # funkcja wypisująca po kolei wszystkie dodane wydatki
    def wyswietl_wszystkie(self):
        number = 1
        for o in self.lista_produktow:
            print(str(number) + ". " + o.__str__())
            number += 1
    def wyswietl_uslugi(self):
        number = 1
        for o in self.lista_produktow:
            if o.typ == "usluga":
                print(str(number) + ". " + o.__str__())
                number += 1
    def wyswietl_po_dacie(self,rok,miesiac,dzien):
        data = datetime(rok,miesiac,dzien)
        number = 1
        for o in self.lista_produktow:
            roznica_dni_od_danej_daty = (o.data - data).days
            if(roznica_dni_od_danej_daty > 0):
                print(str(number) + ". " + o.__str__())
                number += 1
    def wyswietl_po_kwocie(self,kwota):
        number = 1
        for o in self.lista_produktow:
            if(o.kwota > kwota):
                print(str(number) + ". " + o.__str__())
                number += 1
    def wyswietl_wydatek_danego_typu_daty(self,typ,rok,miesiac,dzien):
        number = 1
        data = datetime(rok,miesiac,dzien)
        for o in self.lista_produktow:
            roznica_dni_od_danej_daty = (o.data - data).days
            if roznica_dni_od_danej_daty > 0 and o.typ == typ:
                print(str(number) + ". " + o.__str__())
                number += 1
    def zapisz_wydatki_do_pliku(self):
        file = open("zapis.txt","w")
        file.write("")
        file.close()
        for o in self.lista_produktow:
            file = open("zapis.txt","a")
            file.writelines(str(o) + "\n")
            file.close()
    def wypisz_wydatki_z_pliku(self):
        numbers = 1
        file = open("zapis.txt")
        Lines = file.readlines()
        for line in Lines:
            print(str(numbers) + ". " + line.strip())
            numbers += 1
    def menu(self):
            print("cos")
ob1 = Wydatek("lays",4.50,"towar",datetime(2015,4,17))
ob2 = Wydatek("coca cola",6,"towar",datetime(2021,12,17))
ob3 = Wydatek("fryzjer",25,"usluga",datetime(2021,10,2))
ob4 = Wydatek("samochod",10000,"towar",datetime(2022,2,14))
ob5 = Wydatek("woda",2,"towar",datetime(2022,2,15))
ob6 = Wydatek("lawka",1500,"towar",datetime(2005,12,6))


lista_str=[]
lista_str.append(str(ob1.__dict__))
lista_str.append(str(ob2.__dict__))
print(lista_str)



a_file = open("zapis.txt")

file_contents = a_file.read()


print(file_contents)