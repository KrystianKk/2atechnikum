from calendar import month
from contextlib import nullcontext
from email.mime import application
import numbers
import os
import datetime
from time import time
import sys

# aktualna data
Time = datetime.datetime.now()
# listy przechowujące wydatki
wydatek = []
listalist = []


# klasa Produktu z atrybutami kwoty, dnia, miesiąca, roku, typu
class Produkt:
    def __init__(self, kwota, dzien,miesiac,rok,typ = ["towar","usluga"]):
        self.kwota = kwota
        self.dzien = dzien
        self.miesiac = miesiac
        self.rok = rok
        self.typ = typ
    # funkcja wypisująca wszystkie informacje po kolei
    def GetInfo(self):
        return ("typ: %s data: %d.%d.%d kwota: %.2f"%(self.typ,self.rok,self.miesiac,self.dzien,self.kwota))

# klasa Lista z atrybutem lista_produktow przechowująca każdy dodany wydatek
class Lista_Produktów:
    def __init__(self,lista_produktow):
        self.lista_produktow = lista_produktow
    
    # funkcja wypisująca po kolei wszystkie dodane wydatki
    def WyswietlWszystkie(self):
        number = 1
        for info in self.lista_produktow:
            print(str(number) + ". " + info.GetInfo())
            number += 1
        goBack()

    # zaawansowana funkcja wypisująca tylko wydatki z przed 3 ostatnich miesięcy i tylko usługi
    def ZaawansowaneWyswietlanie(self):
        number = 1
        _miesiac_ = Time.month + 9
        _rok_ = Time.year - 1
        for info in self.lista_produktow:
            if(info.miesiac >= Time.month - 3 and info.rok == Time.year and info.typ == "usluga"):
                print(str(number) + ". " + info.GetInfo())
                number += 1
            elif(info.miesiac >= _miesiac_ and info.rok == _rok_ and info.typ == "usluga"):
                print(str(number) + ". " + info.GetInfo())
                number += 1
        goBack()
 

# funkcja powodująca wyjście z programu
def wyjscie():
    sys.exit()

# funkcja czyszczenia konsoli
def screen_clear():
   # for mac and linux(here, os.name is 'posix')
   if os.name == 'posix':
      _ = os.system('clear')
   else:
      # for windows platfrom
      _ = os.system('cls')

# funkcja powrotu do menu głównego
def goBack():
    x = input("if you want to go back click enter ")
    if(x != None):
        menu()

# funkcja menu głównego gdzie znajdują się wszystkie opcje dodawania wydatków,
# podstawowego przeglądania wydatków, zaawansowanego przeglądania wydatków
# i wyjścia. Opcje wybiera się przez wpisywanie numerka obok podanej opcji 
def menu():
    screen_clear()
    print("1.dodaj wydatek")
    print("2.pokaż historie wzsystkich wydatkow")
    print("3.pokaz wszystkie uslugi z ostatnich 3 miesiecy")
    print("4.wyjdz")
    x = int(input("podaj numer opcji zeby ja wybrac: "))
    if(x == 1):
        dodajWydatek()
    elif(x == 2):
        # jesli nie ma żadnego dodanego wydatku wyświetli się wiadomość "nie posiadasz wydatków"
        # w innym przypadku zostanie wykonana funkcja które wyświetla wszystkie wydatki
        # które dodało się do listy listalist
        if(len(wydatek) < 1):
            print("nie posiadasz wydatkow")
            menu()
        else:
            lista = Lista_Produktów(listalist)
            lista.WyswietlWszystkie()
    elif(x == 3):
        if(len(wydatek) < 1):
            print("nie posiadasz wydatkow")
            menu()
        else:
            lista = Lista_Produktów(listalist)
            lista.ZaawansowaneWyswietlanie()
    # wyjście z programu
    elif(x == 4):
        wyjscie()
    # została wybrana nie prawidłowa opjca więc program zostanie zamknięty
    else:
        print("nie ma takiej opcji")
        wyjscie()

# funkcja dodająca do list wpisane przez użytkownika wydatki        
def dodajWydatek():
    typ = input("podaj typ wydatku (towar/usluga): ")
    rok = int(input("podaj rok: "))
    miesiac = int(input("podaj miesiac: "))
    dzien = int(input("podaj dzien: "))
    kwota = float(input("podaj kwote wydatku: "))
    objekt = Produkt(kwota,dzien,miesiac,rok,typ)
    wydatek.append(objekt)
    listalist.append(wydatek[-1])
    menu()
objekt = Produkt(0,0,0,0,"")
print("dzien dobry")
print()
menu()



class Wydatek:
    def __init__(self,nazwa,kwota,typ="towar"):
        self.nazwa = nazwa
        self.kwota = kwota
        self.typ = typ
        
    def __str__(self):
        return(self.nazwa + "" +)
    str(self.kwota) + "" + (self.typ)

ob1 = Wydatek("lays",4,50)
ob2 = Wydatek("coca cola",6)
ob3 = Wydatek("fryzier",25,"usluga")

print(ob1.__dict__)
print(ob2)
print(ob3)

